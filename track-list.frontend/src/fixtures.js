export default[{
        "idSinger": 1,
        "singer": "A-Ha",
        "idTrack": 1,
        "track": "A Little Bit",
        "idGenre": 1,
        "genre": "Pop",
        "idYear": 5,
        "year": "1984"
    }, {
        "idSinger": 7,
        "singer": "Offspring",
        "idTrack": 61,
        "track": "A Thousand Days",
        "idGenre": 4,
        "genre": "Punk",
        "idYear": 20,
        "year": "1999"
    }, {
        "idSinger": 1,
        "singer": "A-Ha",
        "idTrack": 2,
        "track": "Afternoon High",
        "idGenre": 1,
        "genre": "Pop",
        "idYear": 4,
        "year": "1983"
    }, {
        "idSinger": 3,
        "singer": "Eminem",
        "idTrack": 21,
        "track": "Airplanes",
        "idGenre": 2,
        "genre": "Rap",
        "idYear": 21,
        "year": "2000"
    }]