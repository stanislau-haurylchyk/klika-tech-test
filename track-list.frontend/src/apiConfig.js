export const apiUrl = 'http://localhost:8000';

export const apiRoutes = {
    count: '/count',
    singer: '/singer-filter',
    genre: '/genre-filter',
    year: '/year-filter',
    main: '/field-t.track/sort-asc/page-0'
};