import React, {Component} from 'react';
import TrackList from '../TrackList';
import Filter from '../Filter';
import { Link } from 'react-router-dom';
import { apiRoutes } from '../../apiConfig';
import 'bootstrap/dist/css/bootstrap.css';
import './style.css';

class PlayList extends Component {
    state = {
        isASCs: true,
        isASCt: true,
        isASCg: true,
        isASCy: true
    }
    
    componentDidMount() {
        if (!this.props.tracks) {
            this.props.loadTracks();
        }
        if (!this.props.count) {
            this.props.loadTracksCount();
        }
    }

    render() {
        const {tracks, count} = this.props;
        const trackElements = !tracks ? null : tracks.map(track => 
                                <TrackList key={track.idTrack} {...track} />
                            );
        return(
                <div className='play-list'>
                    <h4>Плейлист</h4>
                    <table>
                        <tbody>
                            <tr>
                                <th>Исполнитель <button  onClick={this.sortSingerClick}>{this.state.isASCs ? 'A-Z' : 'Z-A'}</button></th>
                                <th>Песня <button onClick={this.sortTrackClick}>{this.state.isASCt ? 'A-Z' : 'Z-A'}</button></th>
                                <th>Жанр <button onClick={this.sortGenreClick}>{this.state.isASCg ? 'A-Z' : 'Z-A'}</button></th>
                                <th>Год <button onClick={this.sortYearClick}>{this.state.isASCy ? 'A-Z' : 'Z-A'}</button></th>
                            </tr>                        
                            {trackElements}
                        </tbody>
                    </table>
                </div>
                )
    }
    sortSingerClick = () => {
        this.setState({
            isASCs: !this.state.isASCs
        })
    }
    sortTrackClick = () => {
        this.setState({
            isASCt: !this.state.isASCt
        })
    }
    sortGenreClick = () => {
        this.setState({
            isASCg: !this.state.isASCg
        })
    }
    sortYearClick = () => {
        this.setState({
            isASCy: !this.state.isASCy
        })
    }
}


export default PlayList;