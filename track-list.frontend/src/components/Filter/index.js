import React, {Component} from 'react';
import Option from '../Option';
import './style.css';

class Filter extends Component {
    constructor(props) {
        super(props);
        this.state = {
            filter: null
        };
    }
    componentDidMount() {
        if (!this.props.singers) {
            this.props.loadSingersFilter();
        }
        if (!this.props.genres) {
            this.props.loadGenresFilter();
        }
        if (!this.props.years) {
            this.props.loadYearsFilter();
        }
    }
    
    render() {
        const {singers, genres, years} = this.props;
        const singerFilter = !singers ? null : singers.map(singer => 
                                <Option key={singer.idSinger} filter = {singer.singer} />
                            );
        const genreFilter = !genres ? null : genres.map(genre => 
                                <Option key={genre.idGenre} filter = {genre.genre} />
                            );
        const yearFilter = !years ? null : years.map(year => 
                                <Option key={year.idYear} filter = {year.year} />
                            );
        return(
                <div className='filter'>
                    <h4 className='filter__h4'>Фильтр</h4>
                    <table>
                        <tbody>
                            <tr>
                                <td>Исполнитель</td>
                            </tr>
                            <tr>
                                <td>
                                    <select>
                                        <option>Все</option>
                                        {singerFilter}
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td>Жанр</td>
                            </tr>
                            <tr>
                                <td>
                                    <select>
                                        <option>Все</option>
                                        {genreFilter}
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td>Год</td>
                            </tr>
                            <tr>
                                <td>
                                    <select>
                                        <option>Все</option>
                                        {yearFilter}                    
                                    </select>
                                </td>                        
                            </tr>
                        </tbody>
                    </table>
                </div>
                )
    }
}
export default Filter;