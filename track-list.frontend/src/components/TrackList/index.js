import React from 'react';
import './style.css';

function TrackList(props) {
    return(
            <tr className="track-list__tr">
                <td className="track-list__td">{props.singer}</td>
                <td className="track-list__td">{props.track}</td>
                <td className="track-list__td">{props.genre}</td>
                <td className="track-list__td">{props.year}</td>
            </tr>
            )

}

export default TrackList;
