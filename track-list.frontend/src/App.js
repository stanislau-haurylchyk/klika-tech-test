import React, { Component } from 'react';
import { Router, Route, Switch, Link } from 'react-router-dom';
import createHistory from 'history/createHashHistory';
import { apiUrl, apiRoutes } from './apiConfig';
import Filter from './components/Filter';
import PlayList from './components/PlayList';
import './App.css';
import 'bootstrap/dist/css/bootstrap.css';

const history = createHistory();

class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            count: null,
            singers: null,
            genres: null,
            years: null,
            tracks: null
        };
    }
    loadTracks = () => {
        fetch(apiUrl + apiRoutes.main)
                .then(res => res.json())
                .then(res => this.setState({tracks: res}))
                .catch(error => {
                    alert('Ошибка при получении списка треков');
                    console.error(error);
                });
    }
    // Получаем количество треков (для постраничной навигации)
    loadTracksCount = () => {
        fetch(apiUrl + apiRoutes.count)
                .then(res => res.json())
                .then(res => this.setState({count: res}))
                .catch(error => {
                    alert('Ошибка при получении количества треков');
                    console.error(error);
                });
    }
    // Получаем данные для фильтра "Исполнитель"
    loadSingersFilter = () => {
        fetch(apiUrl + apiRoutes.singer)
                .then(res => res.json())
                .then(res => this.setState({singers: res}))
                .catch(error => {
                    alert('Ошибка при получении списка исполнителей');
                    console.error(error);
                });
    }
    // Получаем данные для фильтра "Жанр"
    loadGenresFilter = () => {
        fetch(apiUrl + apiRoutes.genre)
                .then(res => res.json())
                .then(res => this.setState({genres: res}))
                .catch(error => {
                    alert('Ошибка при получении списка жанров');
                    console.error(error);
                });
    }
    // Получаем данные для фильтра "Год"
    loadYearsFilter = () => {
        fetch(apiUrl + apiRoutes.year)
                .then(res => res.json())
                .then(res => this.setState({years: res}))
                .catch(error => {
                    alert('Ошибка при получении списка годов');
                    console.error(error);
                });
    }
    render() {
        const {tracks, count, singers, genres, years} = this.state; 
        return(
                <div className='container'>
                <Filter loadYearsFilter={this.loadYearsFilter} singers={singers} loadGenresFilter={this.loadGenresFilter} genres={genres} loadSingersFilter={this.loadSingersFilter} years={years} />
                    <Router history={history}>
                            <Switch>
                                <Route exact path='/'>
                                    <PlayList loadTracks={this.loadTracks} tracks={tracks} loadTracksCount={this.loadTracksCount} count={count}/>
                                </Route>
                            </Switch>
                    </Router>
                </div>
);
    }
}
export default App;
