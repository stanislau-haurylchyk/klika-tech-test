<?php

namespace Music\TrackListBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="year")
 */
class Year {
    /**
     * @ORM\Id
     * @ORM\Column(type="integer", name="id_year")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $idYear;

    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $year;

    /**
     * Get idYear
     *
     * @return integer
     */
    public function getIdYear()
    {
        return $this->idYear;
    }

    /**
     * Set year
     *
     * @param string $year
     *
     * @return Year
     */
    public function setYear($year)
    {
        $this->year = $year;

        return $this;
    }

    /**
     * Get year
     *
     * @return string
     */
    public function getYear()
    {
        return $this->year;
    }
}
