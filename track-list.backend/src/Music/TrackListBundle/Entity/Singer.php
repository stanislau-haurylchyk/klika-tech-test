<?php

namespace Music\TrackListBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="singer")
 */
class Singer {
    /**
     * @ORM\Id
     * @ORM\Column(type="integer", name="id_singer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $idSinger;

    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $singer;

    /**
     * Get idSinger
     *
     * @return integer
     */
    public function getIdSinger()
    {
        return $this->idSinger;
    }

    /**
     * Set singer
     *
     * @param string $singer
     *
     * @return Singer
     */
    public function setSinger($singer)
    {
        $this->singer = $singer;

        return $this;
    }

    /**
     * Get singer
     *
     * @return string
     */
    public function getSinger()
    {
        return $this->singer;
    }
}
