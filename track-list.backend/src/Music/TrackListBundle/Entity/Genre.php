<?php

namespace Music\TrackListBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="genre")
 */
class Genre {

    /**
     * @ORM\Id
     * @ORM\Column(type="integer", name="id_genre")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $idGenre;

    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $genre;


    /**
     * Get idGenre
     *
     * @return integer
     */
    public function getIdGenre()
    {
        return $this->idGenre;
    }

    /**
     * Set genre
     *
     * @param string $genre
     *
     * @return Genre
     */
    public function setGenre($genre)
    {
        $this->genre = $genre;

        return $this;
    }

    /**
     * Get genre
     *
     * @return string
     */
    public function getGenre()
    {
        return $this->genre;
    }
}
