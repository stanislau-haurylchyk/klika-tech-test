<?php

namespace Music\TrackListBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class TrackListController extends Controller {

    public function getTracksCountAction() {
        // Вызов Entity Manager
        $em = $this->getDoctrine()->getManager();
        // Запрос к БД
        $query = $em->createQuery('SELECT COUNT(t) FROM Music\TrackListBundle\Entity\Track t');

        // Количество записей в таблице Track
        $count = $query->getResult();

        // Формируем ответ
        $response = new Response(json_encode($count));
        $response->headers->set('Access-Control-Allow-Origin', '*');
        $response->headers->set('Access-Control-Allow-Methods', 'GET');
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }

    public function mainAction($sbrack = '', $idSinger = '', $gbrack = '', $idGenre = '', $ybrack = '', $idYear = '', $field = 't.track', $sort = 'ASC', $offset = 0, $limit = 10) {
        if ($idSinger > 0) {
            $sbrack = '(';
            $idSinger = ' AND s.idSinger=' . $idSinger . ')';
        }
        if ($idGenre > 0) {
            $gbrack = '(';
            $idGenre = ' AND g.idGenre=' . $idGenre . ')';
        }
        if ($idYear > 0) {
            $ybrack = '(';
            $idYear = ' AND y.idYear=' . $idYear . ')';
        }

        // Вызов Entity Manager
        $em = $this->getDoctrine()->getManager();
        // Запрос к БД
        $query = $em->createQuery('SELECT s.idSinger, s.singer, t.idTrack, t.track, g.idGenre, g.genre, y.idYear, y.year FROM Music\TrackListBundle\Entity\Singer s
		JOIN Music\TrackListBundle\Entity\Track t WHERE ' . $sbrack . 's.idSinger=t.idSinger' . $idSinger .
                ' JOIN Music\TrackListBundle\Entity\Genre g WHERE ' . $gbrack . 'g.idGenre=t.idGenre' . $idGenre .
                ' JOIN Music\TrackListBundle\Entity\Year  y WHERE ' . $ybrack . 'y.idYear=t.idYear' . $idYear . ' ORDER BY ' . $field . ' ' . $sort);

        $query->setFirstResult($offset);
        $query->setMaxResults($limit);

        // Массив с данными
        $data = $query->getResult();
        if (!$data) {
            throw $this->createNotFoundException('Unable to find Tracks!');
        }
        // Формируем ответ JSON
        $response = new Response(json_encode($data));
        $response->headers->set('Access-Control-Allow-Origin', '*');
        $response->headers->set('Access-Control-Allow-Methods', 'GET');
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }

    public function getSingerFilterAction() {
        // Вызов Entity Manager
        $em = $this->getDoctrine()->getManager();
        // Запрос к БД
        $query = $em->createQuery('SELECT s.idSinger, s.singer FROM Music\TrackListBundle\Entity\Singer s');

        // Массив с данными
        $data = $query->getResult();
        if (!$data) {
            throw $this->createNotFoundException('Unable to find Singers!');
        }
        // Формируем ответ JSON
        $response = new Response(json_encode($data));
        $response->headers->set('Access-Control-Allow-Origin', '*');
        $response->headers->set('Access-Control-Allow-Methods', 'GET');
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }

    public function getGenreFilterAction() {
        // Вызов Entity Manager
        $em = $this->getDoctrine()->getManager();
        // Запрос к БД
        $query = $em->createQuery('SELECT g.idGenre, g.genre FROM Music\TrackListBundle\Entity\Genre g');

        // Массив с данными
        $data = $query->getResult();
        if (!$data) {
            throw $this->createNotFoundException('Unable to find Singers!');
        }
        // Формируем ответ JSON
        $response = new Response(json_encode($data));
        $response->headers->set('Access-Control-Allow-Origin', '*');
        $response->headers->set('Access-Control-Allow-Methods', 'GET');
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }

    public function getYearFilterAction() {
        // Вызов Entity Manager
        $em = $this->getDoctrine()->getManager();
        // Запрос к БД
        $query = $em->createQuery('SELECT y.idYear, y.year FROM Music\TrackListBundle\Entity\Year y');

        // Массив с данными
        $data = $query->getResult();
        if (!$data) {
            throw $this->createNotFoundException('Unable to find Singers!');
        }
        // Формируем ответ JSON
        $response = new Response(json_encode($data));
        $response->headers->set('Access-Control-Allow-Origin', '*');
        $response->headers->set('Access-Control-Allow-Methods', 'GET');
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }

}
